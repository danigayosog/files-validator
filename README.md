# Files validator

![pipeline](https://gitlab.com/danigayosog/files-validator/badges/master/pipeline.svg)

Files validator is a simple Django app to validate uploaded files.


## Quick start

1. Add "files_validator" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'files_validator',
    ]

2. Include the polls URLconf in your project urls.py like this::

    path('validator/', include('files_validator.urls')),

3. Run `python manage.py migrate` to create the files validator models.

4. Ensure to set  `MEDIA_ROOT` and `MEDIA_URL` for seve media files with django

5. Start the development server and visit http://127.0.0.1:8000/validator/

6. Add new validators in settings with property FILE_VALIDATORS which is an array
  of strings og dotted path packages like `FILE_VALIDATORS = [mypackage.validator]`

## Development
In order to develop further this project and make migration you need a functional django project. So you can use the 
project under `example`. For create new migrations use the command `python manage.py makemigrations` under this 
directory. In addition, you need to install the package with `pip install -e ./`.

### Test
```bash
python runtests.py
```

### Build and install
```bash
python setup.py sdist
pip install dist/django-files-validator-x.x.x.tar.gz
```

### Example server
An example server is under `example/` directory, to try this project run:
```bash
pip install -e ./
cd example/
python manage.py migrate
python manage.py runserver:8000
```

### Install from this repository
```bash
pip install -e git+https://gitlab.com:danigayosog/files-validator.git#egg=django-files-validator
```

After that with `pip freeze` it'll appear the url and the commit to reference. It would be a good idea to link 
the previous link to a fixed version of project like `files-validator.git@tag_name#egg`. In addition the freeze 
instruction get the hash of commit, so if you want a new version you should use the previous command `pip install -e` to
get the new version.