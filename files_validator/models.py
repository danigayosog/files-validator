from pathlib import Path

from django.core.files.storage import FileSystemStorage
from django.db import models


def get_upload_path(instance, filename):
    return path_from_path_with_pound(filename)


def path_from_path_with_pound(name: str) -> str:
    if '#' not in name:
        return Path(name).as_posix()
    return Path(*name.split('#')).as_posix()


# Create your models here.
class OverwriteStorage(FileSystemStorage):

    def get_available_name(self, name, max_length=None):
        if self.exists(name):
            self.delete(name)
        return name


class FileHandler(models.Model):
    file = models.FileField(upload_to=get_upload_path, storage=OverwriteStorage())
