from django.apps import AppConfig


class FilesHandlerConfig(AppConfig):
    name = 'files_validator'
    verbose_name = 'Files validator'
