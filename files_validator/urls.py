from django.urls import path
from django.views.generic import RedirectView

from files_validator import views

urlpatterns = [
    path('', RedirectView.as_view(url='upload-files/')),
    path('upload-files/', views.FileList.as_view()),
    path('upload-files/files.json', views.APIFileList.as_view()),
    path('upload-files/validators.json', views.APIValidators.as_view()),
    path('upload/', views.APIUploadFiles.as_view()),
    path('delete/', views.APIDeleteFileList.as_view()),
]
