from typing import List
from unittest.mock import Mock

from django.core.files.uploadedfile import InMemoryUploadedFile
from django.test import TestCase, override_settings

# Create your tests here.
from files_validator.file_validators import ValidatorFactory, FileValidator, InvalidFileError
from files_validator.models import path_from_path_with_pound


class ExampleFileValidator(FileValidator):
    validator_name = 'example'
    valid_extensions = ['.txt']

    def validate_file(self, filename: InMemoryUploadedFile) -> List[str]:
        return []


class Tests(TestCase):

    def test_validator_fails(self):
        with self.assertRaises(ValueError):
            ValidatorFactory.get_instance('invalid validator')

    def test_validator_file_name(self):
        validator = ValidatorFactory.get_instance('example')

        try:
            validator.validate_file_name('some/route/example.txt')
            validator.validate_file_name('some/route/example.TXT')
        except InvalidFileError:
            self.fail('Calls should not raise error')

        with self.assertRaises(InvalidFileError):
            validator.validate_file_name('some/route/example.txt2')

    def test_validator_validate(self):
        validator = ValidatorFactory.get_instance('example')

        test_file = Mock()
        self.assertEqual([], validator.validate_file(test_file), 'Should return no errors')

    def test_path(self):
        self.assertEqual('imagenes/archivo1.jpeg', path_from_path_with_pound('imagenes#archivo1.jpeg'))
        self.assertEqual('nested/imagenes/archivo1.jpeg', path_from_path_with_pound('nested#imagenes#archivo1.jpeg'))
        self.assertEqual('archivo1.jpeg', path_from_path_with_pound('archivo1.jpeg'))
