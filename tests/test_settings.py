SECRET_KEY = 'fake-key'

INSTALLED_APPS = [
    'files_validator',
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'database.db',
    }
}

FILE_VALIDATORS = ['tests.tests.ExampleFileValidator']
